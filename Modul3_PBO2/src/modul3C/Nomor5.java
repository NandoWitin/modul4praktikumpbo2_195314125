package modul3C;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Nomor5 extends JDialog {

    private JTextArea textArea;
    private JCheckBox cBoxCentered, cBoxBold, cBoxItalic;
    private JButton buttonLeft, buttonRight;
    private JRadioButton rButtonRed, rButtonGreen, rButtonBlue;

    public Nomor5() {
        setLayout(null);
        setTitle("RadioButtonDemo");
        setSize(450, 200);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);

        textArea = new JTextArea();
        textArea.setText("Welcome to Java");
        textArea.setBounds(80, 1, 270, 95);
        this.add(textArea);

        buttonLeft = new JButton("Left");
        buttonLeft.setBounds(130, 110, 70, 25);
        this.add(buttonLeft);
        buttonRight = new JButton("Right");
        buttonRight.setBounds(210, 110, 70, 25);
        this.add(buttonRight);

        cBoxCentered = new JCheckBox("Centered");
        cBoxCentered.setBounds(350, 1, 100, 30);
        this.add(cBoxCentered);
        cBoxBold = new JCheckBox("Bold");
        cBoxBold.setBounds(350, 30, 100, 30);
        this.add(cBoxBold);
        cBoxItalic = new JCheckBox("Italic");
        cBoxItalic.setBounds(350, 60, 100, 30);
        this.add(cBoxItalic);

        rButtonRed = new JRadioButton("Red");
        rButtonRed.setBounds(1, 1, 60, 30);
        rButtonGreen = new JRadioButton("Green");
        rButtonGreen.setBounds(1, 30, 60, 30);
        rButtonBlue = new JRadioButton("Blue");
        rButtonBlue.setBounds(1, 60, 60, 30);
        this.add(rButtonRed);
        this.add(rButtonGreen);
        this.add(rButtonBlue);
        ButtonGroup butGrup = new ButtonGroup();
        butGrup.add(rButtonRed);
        butGrup.add(rButtonGreen);
        butGrup.add(rButtonBlue);

    }

    public static void main(String[] args) {
        new Nomor5();
    }
}
